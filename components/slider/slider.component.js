app.component('slider', {
  templateUrl: './dist/slider/slider.html',
  controllerAs: 'slider',
  controller: 'sliderController',
  bindings: {
    data: '<',
  },
});

app.controller('sliderController', [ function(  ) {

  this.images = [];
  this.position = 0;
  let size = 0;

  this.$onChanges = () => {
    this.images = this.data;
    size = this.data.length - 1;
    this.position = 0;
  };

  this.next = () => { this.position = this.position +1 > size ? 0 : this.position +1; };

  this.prev = () => { this.position = this.position -1 < 0 ? size : this.position -1; };

}]);
