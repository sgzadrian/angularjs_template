app.component('palette', {
  template: `
  <div class="palette w-100 d-flex flex-wrap">
    <div ng-repeat="color in palette.colors" class="palette-item w-50 {{ 'bg-' + color }}" ></div>
  </div>
  `,
  controllerAs: 'palette',
  controller: function paletteController() {

    this.colors = [
      'red',
      'pink',
      'purple',
      'indigo',
      'blue',
      'cyan',
      'green',
      'yellow',
      'orange',
      'brown',
      'grey',
      'blue_gray'
    ];

  }
});
