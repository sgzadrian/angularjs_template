const app = angular.module("App", [ 'ngRoute', 'ngAnimate', 'pascalprecht.translate' ]);

const local = true;
const prod = false;

app.config(['$compileProvider', '$routeProvider', '$locationProvider', '$translateProvider', '$httpProvider',
function($compileProvider, $routeProvider, $locationProvider, $translateProvider, $httpProvider) {

  $compileProvider.debugInfoEnabled(false);
  $compileProvider.commentDirectivesEnabled(false);

  $routeProvider
  .when('/', {
    templateUrl: './dist/home/home.html',
  })
  .when('/contact', {
    templateUrl: './dist/contact/contact.html',
  })
  .otherwise({ redirectTo: '/' });

  $locationProvider.html5Mode(true);

  $translateProvider.translations('es', es_translation);
  $translateProvider.translations('en', en_translation);
  $translateProvider.useSanitizeValueStrategy('escape');
  $translateProvider.preferredLanguage('es');

  $httpProvider.useApplyAsync(true);
  $httpProvider.defaults.headers.common = {};
  $httpProvider.defaults.headers.post = {};
}]);
