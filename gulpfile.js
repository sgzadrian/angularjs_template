var gulp = require('gulp');
var concat = require('gulp-concat');
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
var changed = require('gulp-changed');
var browserSync = require('browser-sync').create();
var path = require('path');

var babel = require('gulp-babel');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
sass.compiler = require('dart-sass');
var htmlmin = require('gulp-htmlmin');

var htmlmin_opt = {
  collapseWhitespace: true,
  collapseInlineTagWhitespace: true,
  collapseBooleanAttributes: true,
  removeComments: true
};

gulp.task('app', function() {
  return gulp.src([
    'app.module.js',
    'services/*.*.js',
    'app/*.*.js',
    'app/*/*.*.js'
  ])
  .pipe(plumber())
  .pipe(concat('app.min.js', { newLine: ';' }))
  .pipe(babel({ presets: [ '@babel/preset-env' ] }))
  .pipe(uglify({ mangle: true }))
  .pipe(plumber.stop())
  .pipe(gulp.dest('./dist'));
});

gulp.task('components', function() {
  gulp.src('components/*/*.html')
  .pipe(plumber())
  .pipe(htmlmin(htmlmin_opt))
  .pipe(plumber.stop())
  .pipe(gulp.dest('./dist'));

  return gulp.src([
    'components/*.*.js',
    'components/*/*.*.js',
  ])
  .pipe(plumber())
  .pipe(concat('components.min.js', { newLine: ';' }))
  .pipe(babel({ presets: [ '@babel/preset-env' ] }))
  .pipe(uglify({ mangle: true }))
  .pipe(plumber.stop())
  .pipe(gulp.dest('./dist'));
});

gulp.task('sass', function() {
  gulp.src('styles.scss')
  .pipe(plumber())
  .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
  .pipe(gulp.dest('./dist'))
  .pipe(plumber.stop());
});

gulp.task('templates', function() {
  return gulp.src('app/*/*.html')
  .pipe(plumber())
  .pipe(htmlmin(htmlmin_opt))
  .pipe(plumber.stop())
  .pipe(gulp.dest('./dist'));
});

gulp.task('reload', function() { browserSync.reload() });

gulp.task('inject', function() { browserSync.reload('./dist/styles.css'); });

gulp.task('serve', [ 'templates', 'sass', 'components', 'app' ], function() {

  browserSync.init({
    server: { baseDir: './' },
    open: false,
    single: true
  });

  process.on('exit', function() { browserSync.exit(); });

  gulp.watch([
    'styles.scss',
    'sass/*.scss',
    'app/*.scss',
    'app/*/*.scss',
    'components/*.scss',
    'components/*/*.scss',
    'libs/bootstrap/scss/bootstrap.scss'
  ], [ 'sass', 'inject' ]);

  gulp.watch([
    'app.module.js',
    'services/*.*.js',
    'app/*.*.js',
    'app/*/*.*.js'
  ], [ 'app', 'reload' ]);

  gulp.watch([
    'components/*/*.*.js',
    'components/*/*.html'
  ], [ 'components', 'reload' ]);

  gulp.watch([
    'index.html',
    'app/*/*.html'
  ], [ 'templates', 'reload' ]);

  gulp.watch('i18n/*.js', [ 'reload' ]);

});

gulp.task('default', [ 'serve' ]);
