# AngularJS Template

This project was created with common features for simple web pages, it includes:
* Gulp
* SASS
* Bootstrap
* Google Analythics

Also contains the basics to communicate with REST services, to work with translations and work with components.