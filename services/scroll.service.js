app.factory('scroll', [ '$timeout', function( $timeout ) {
  return {
    to: ( id, y = 0 ) => {
      let elem = document.getElementById( id );
      let pos = elem.scrollTop;
      console.log(pos);
      if ( pos < 200 ) { elem.scrollTo(0, y); return; }
      let steps = 25;
      let time = 500 / steps;
      let dist = pos / time;
      for ( let i = 1; i <= steps; i++ ) {
        $timeout(() => {
          elem.scrollTo(0, pos - dist * i);
        }, i * time);
      }
    },
  };
}]);
