app.service('api', [ '$http', function( $http ) {

  this.base_path = local ? 'https://' : 'http://localhost/';

  this.path = this.base_path + 'mobile/';

  this.fetch =  ( endpoint = '' )       => { return $http({ method: 'GET',    url: this.path + endpoint }); };

  this.get =    ( endpoint, id )   => { return $http({ method: 'GET',    url: this.path + endpoint + '/' + id  }); };

  this.post =   ( endpoint, data ) => { return $http({ method: 'POST',   url: this.path + endpoint, data: data }); };

}]);
