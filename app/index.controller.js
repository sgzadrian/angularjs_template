app.controller('indexController', ['$scope', '$translate', '$window',
function($scope, $translate, $window) {

  this.ready = false;

  this.state = {
    lang: 'es',
  };

  this.init = () => {
    if ( localStorage.lang ) {
      this.state.lang = localStorage.lang;
    }
  }

  this.toggleBody = () => {
    if ( !this.ready ) { this.ready = true; }
  }

  $scope.changeLang = ( new_lang ) => {
    $translate.use( new_lang );
    localStorage.lang = new_lang;
  }

  $scope.sanitize = ( str ) => { return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '').replace(/ /g, '%20'); }

}]);
